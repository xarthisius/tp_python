#!/usr/bin/python
import numpy as np

def mandelbrot(h, w, maxit=1000):
    x, y = np.ogrid[-2.5:1:w * 1j, -1:1:h * 1j]
    c = x + y * 1j
    z = c
    output = np.zeros((w, h), dtype=int) + maxit
    for i in xrange(maxit):
        z = z**2 + c
        div = z * z.conj() > 4
        c[div] = 0
        z[div] = 0
        output[div] = i
    return output.T


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    plt.imshow(mandelbrot(512, 512))
    plt.show()
