# build with:
#   python setup.py build_ext --inplace

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

ext_modules = [
    Extension("mandelcyt",
              ["mandelcyt.pyx"],
              extra_compile_args=['-fopenmp'],
              extra_link_args=['-fopenmp'],
    )
]

setup(
  name = 'Mandelbrot set',
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules,
  include_dirs = [numpy.get_include(),],
)
