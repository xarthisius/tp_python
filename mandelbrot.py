import matplotlib.pyplot as plt
from functools import partial
from mpi4py import MPI


def mandelbrotCalcRow(yPos, h, w, max_iteration=1000):
    y0 = yPos * (2.0 / float(h)) - 1  # rescale to -1 to 1
    row = []
    for xPos in range(w):
        x0 = xPos * (3.5 / float(w)) - 2.5  # rescale to -2.5 to 1
        iteration, z = 0, 0 + 0j
        c = complex(x0, y0)
        while abs(z) < 2 and iteration < max_iteration:
            z = z**2 + c
            iteration += 1
        row.append(iteration)

    return row


def mandelbrotCalcSet(h, w, max_iteration=1000):

    comm = MPI.COMM_WORLD
    partialCalcRow = partial(
        mandelbrotCalcRow, h=h, w=w, max_iteration=max_iteration)

    if comm.Get_rank() == 0:
        lrange = range(h)
        chunks = [[] for _ in range(comm.Get_size())]
        for i, chunk in enumerate(lrange):
            chunks[i % comm.Get_size()].append(chunk)
    else:
        lrange = None
        chunks = None
    lrange = comm.scatter(chunks, root=0)
    mandelImg = map(partialCalcRow, lrange)
    mandelImg = comm.gather(mandelImg, root=0)

    return mandelImg


HSIZE = 400
WSIZE = 400
mandelImg = mandelbrotCalcSet(HSIZE, WSIZE)

COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
SIZE = COMM.Get_size()
if RANK == 0:
    import numpy as np
    img = np.zeros((HSIZE, WSIZE))
    for i in range(HSIZE):
        img[i, :] = mandelImg[i % SIZE][i / SIZE][:]
    plt.imshow(img)
    plt.show()
