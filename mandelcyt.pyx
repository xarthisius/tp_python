import numpy as np
from cython.parallel import parallel, prange

cimport cython
cimport numpy as np
@cython.boundscheck(False)
def mandel_cyt(int h, int w, int maxit=20):
    """Mandelbrot set in Cython"""
    cdef np.ndarray[np.complex128_t, ndim=2] c
    cdef np.ndarray[np.int64_t, ndim=2] output
    cdef int i, j, k
    cdef double complex c0, z

    x, y = np.ogrid[-2.5:1:w * 1j, -1:1:h * 1j]
    c = x + y * 1j
    output = np.zeros((w, h), dtype=int) + maxit
    with nogil, parallel():
        for i in prange(h):
            for j in range(w):
                z = c[i, j]
                c0 = c[i, j]
                for k in xrange(maxit):
                    z = z**2 + c0
                    if (z * z.conjugate()).real > 4.0:
                        output[i, j] = k
                        break
    return output.T
